import imageio
imageio.plugins.ffmpeg.download()
from moviepy.editor import *

FILE_PATH = "some_video.flv"
original_extension = FILE_PATH.split('.')[-1]
output_file_path = FILE_PATH.replace(original_extension, "mp4")

clip = VideoFileClip(FILE_PATH)
clip.write_videofile(output_file_path) # default codec: 'libx264', 24 fps
