import imageio
imageio.plugins.ffmpeg.download()

from moviepy.editor import *
import pygame

pygame.display.set_caption('Video preview using moviepy')

clip = VideoFileClip('some_video.mp4')
clip.preview()

pygame.quit()
