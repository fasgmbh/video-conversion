# Examples for video conversions and display

## Requirements

* Python 2.7
* pygame (pip install pygame)
* imageio (pip install imageio, if not already installed)
* moviepy (pip install moviepy)

Note: **ffmpeg is installed the first time that the download function is called**
